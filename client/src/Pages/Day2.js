import React, { useEffect, useState } from 'react'
import Box from '@material-ui/core/Box'
import Button from '@material-ui/core/Button'
import Container from '@material-ui/core/Container'
import Grid from '@material-ui/core/Grid'
import { Redirect } from 'react-router-dom'
import TextField from '@material-ui/core/TextField'
import Typography from '@material-ui/core/Typography'

const Day2 = props => {

    const [ answer, setAnswer ] = useState(0)
    const [ deposit, setDeposit ] = useState(0)
    const [ goHome, setGoHome ] = useState(false)
    const [ rate, setRate ] = useState(0)
    const [ showAnswer, setShowAnswer ] = useState(false)
    const [ threshold, setThreshold ] = useState(0)

    const handleChangeDeposit = (e) => {
        setDeposit(Number(e.target.value))
    }

    const handleChangeRate = (e) => {
        setRate(Number(e.target.value) / 100)
    }

    const handleChangeThreshold = (e) => {
        setThreshold(Number(e.target.value))
    }

    const handleGoHome = () => {
        setGoHome(true)
    }

    const handleShowAnswer = () => {

        var dep = deposit
        var thresh = threshold
        var myAnswer = answer
        var myRate = rate

        while( dep <= thresh ){
            myAnswer = myAnswer + 1
            dep = dep + ( dep * myRate )
        }
        setAnswer(myAnswer)
        setShowAnswer(true)
    }

    return (
        <Box>
            {goHome ? <Redirect to='/' /> : null}
            <Container>
                <Grid container style={{marginTop: '2rem'}} >
                    <Grid container style={{marginTop: '2rem'}} >
                        <Grid item xs={11}>
                            <Typography variant='h4' style={{width: '100%', textAlign: 'center'}} >
                                Day 2 of JavaScript Christmas
                            </Typography>
                        </Grid>
                        <Grid item xs={1}>
                            <Button variant='contained' onClick={handleGoHome}>
                                Home
                            </Button>
                        </Grid>
                    </Grid>
                    <Grid container style={{marginTop: '2rem'}} >
                        <Typography variant='h6' style={{textAlign: 'left'}} >
                            You have deposited a specific amount of dollars into your bank account.
                            Each year your balance increases at the same growth rate. Find out how
                            long it would take for your balance to pass a specific threshold with the
                            assumption that you don't make any additional deposits.
                        </Typography>
                    </Grid>
                    <Grid container style={{marginTop: '2rem'}} >
                        <Typography variant='h6' style={{textAlign: 'left'}} >
                            For deposit = 100, rate = 20 and threshold = 170, the output should = 3.
                        </Typography>
                    </Grid>
                    <Grid container style={{marginTop: '2rem'}} >
                        <Grid item xs={4}>
                            <TextField label='Enter Deposit Amount' variant='outlined' style={{width: '250px'}} onChange={handleChangeDeposit} />
                        </Grid>
                        <Grid item xs={4}>
                            <TextField label='Enter Rate' variant='outlined' style={{width: '250px'}} onChange={handleChangeRate} />
                        </Grid>
                        <Grid item xs={4}>
                            <TextField label='Enter Threshold' variant='outlined' style={{width: '250px'}} onChange={handleChangeThreshold} />
                        </Grid>
                    </Grid>
                    <Grid container>
                        <Grid item xs={2} style={{marginTop: '2rem', marginLeft: '3.2rem'}} >
                            <Button variant='contained' onClick={handleShowAnswer}>
                                Show Answer
                            </Button>
                        </Grid>
                    </Grid>
                </Grid>
                <Grid style={{marginTop: '2rem'}} >
                    {deposit !== 0 && rate !== 0 ?
                        <Typography variant='h6'>
                            {showAnswer ? `The Answer is: ${answer}  Years` : null}
                        </Typography>
                    : null}
                </Grid>
            </Container>
        </Box>
    )
}

export default Day2
