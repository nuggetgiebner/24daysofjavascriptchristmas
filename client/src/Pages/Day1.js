import React, { useEffect, useState } from 'react'
import Box from '@material-ui/core/Box'
import Button from '@material-ui/core/Button'
import Container from '@material-ui/core/Container'
import Grid from '@material-ui/core/Grid'
import { Redirect } from 'react-router-dom'
import TextField from '@material-ui/core/TextField'
import Typography from '@material-ui/core/Typography'

const Day1 = props => {

    const [ answer, setAnswer ] = useState(0)
    const [ goHome, setGoHome ] = useState(false)
    const [ numberOfCandies, setNumberOfCandies ] = useState(0)
    const [ numberOfChildren, setNumberOfChildren ] = useState(0)

    const handleChangeNumberOfCandies = (e) => {
        setNumberOfCandies(e.target.value)
    }

    const handleChangeNumberOfChildren = (e) => {
        setNumberOfChildren(e.target.value)
    }

    const handleGoHome = () => {
        setGoHome(true)
    }

    useEffect( () => {
        setAnswer(Math.floor(numberOfCandies / numberOfChildren))
    }, [numberOfCandies, numberOfChildren] )

    return (
        <Box>
            {goHome ? <Redirect to='/' /> : null}
            <Container>
                <Grid container style={{marginTop: '2rem'}} >
                    <Grid container style={{marginTop: '2rem'}} >
                        <Grid item xs={11}>
                            <Typography variant='h4' style={{width: '100%', textAlign: 'center'}} >
                                Day 1 of JavaScript Christmas
                            </Typography>
                        </Grid>
                        <Grid item xs={1}>
                            <Button variant='contained' onClick={handleGoHome}>
                                Home
                            </Button>
                        </Grid>
                    </Grid>
                    <Grid container style={{marginTop: '2rem'}} >
                        <Typography variant='h6' style={{textAlign: 'left'}} >
                            n children have got m pieces of candy. They want to eat as much candy as they can,
                            but each child must eat exactly the same amount of candy as an other child.
                            Determine hom many pieces of candy will be eaten by all the children together.
                            Individual pieces of candy cannot be split.
                        </Typography>
                    </Grid>
                    <Grid container style={{marginTop: '2rem'}} >
                        <Typography variant='h6' style={{textAlign: 'left'}} >
                            For n = 3 and m = 10, the output should be 9 candies.
                        </Typography>
                    </Grid>
                    <Grid container style={{marginTop: '2rem'}} >
                        <Grid item xs={6}>
                            <TextField label='Enter Number of Children' variant='outlined' style={{width: '250px'}} onChange={handleChangeNumberOfChildren} />
                        </Grid>
                        <Grid item xs={6}>
                            <TextField label='Enter Number of Candies' variant='outlined' style={{width: '250px'}} onChange={handleChangeNumberOfCandies} />
                        </Grid>
                    </Grid>
                </Grid>
                <Grid style={{marginTop: '2rem'}} >
                    {numberOfCandies !== 0 && numberOfChildren !== 0 ?
                        <Typography variant='h6'>
                            The Answer is: { answer * numberOfChildren }
                        </Typography>
                    : null}
                </Grid>
            </Container>
        </Box>
    )
}

export default Day1
