import React, { useState } from 'react'
import Box from '@material-ui/core/Box'
import Button from '@material-ui/core/Button'
import Container from '@material-ui/core/Container'
import Grid from '@material-ui/core/Grid'
import { Redirect } from 'react-router-dom'
import Typography from '@material-ui/core/Typography'

const Landing = () => {

    const [ goToDay1, setGoToDay1 ] = useState(false)
    const [ goToDay2, setGoToDay2 ] = useState(false)

    const handleDay1 = () => {
        setGoToDay1(true)
    }

    const handleDay2 = () => {
        setGoToDay2(true)
    }

    return (
        <Box>
            {goToDay1 ? <Redirect to='/Day1' /> : null}
            {goToDay2 ? <Redirect to='/Day2' /> : null}
            <Container>
                <Grid container  style={{marginTop: '2rem'}}>
                    <Grid item xs={12}>
                        <Typography variant='h4' >
                            24 Days of JavaScript Christmas
                        </Typography>
                    </Grid>
                </Grid>
                <Grid container  style={{marginTop: '2rem'}}>
                    <Grid item xs={1}>
                        <Button variant='contained' onClick={handleDay1}>
                            <Typography variant='h6' >
                                Day 1
                            </Typography>
                        </Button>
                    </Grid>
                    <Grid item xs={1}>
                        <Button variant='contained' onClick={handleDay2}>
                            <Typography variant='h6' >
                                Day 2
                            </Typography>
                        </Button>
                    </Grid>
                </Grid>
            </Container>
        </Box>
    )
}

export default Landing
