import logo from './logo.svg';
import './App.css';
import Day1 from './Pages/Day1'
import Day2 from './Pages/Day2'
import Landing from './Pages/Landing'
import { Route, Switch } from 'react-router-dom'

function App() {
  return (
    <div className="App">
        <Switch>
          <Route exact path='/'>
            <Landing />
          </Route>
          <Route exact path='/Day1'>
            <Day1 />
          </Route>
          <Route exact path='/Day2'>
            <Day2 />
          </Route>
        </Switch>
    </div>
  );
}

export default App;
